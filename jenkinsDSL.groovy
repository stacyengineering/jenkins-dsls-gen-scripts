job("develop-unittests") {
    def path_to_run = "tests"
    // handle jobs that have 'parameters' list specified
    // if you have custom test job (see above), leave the 
    // parameters below as is:
    parameters {
      // this setup is commonly used for custom tests job:
      gitParam("branch") {
        type("BRANCH")
        defaultValue("configuration-mgnt")
        description("Branch to run tests on")
      }
      stringParam('TEST_PATH', path_to_run, "select a path to run the tests")
    }
    scm {
      git {
        remote {
          url("git@bitbucket.org:stacyengineering/jenkins-dsls-server-side.git")
          // here you specify git credentials to be used when pulling or cloning the repo
          credentials("git-credentials")
        }

        // use master branch by default
        branch("master")
        extensions {
          localBranch("master")
        }
      }
    }
    //todo: add commit number to test suite, add coverage data
    steps {
      shell("""
if ! [ -d ./venv-server-app ]; 
   then virtualenv -p python3.7 venv-server-app;
fi

. ./venv-server-app/bin/activate
pip install -r requirements.txt

pytest -o junit_suite_name=develop-unittest --junitxml=junit_report.xml -s ${path_to_run}
      """)
    }

}

job("develop-ui-build") {
  scm {
      git {
        remote {
          url("git@bitbucket.org:stacyengineering/jenkins-dsls-frontend.git")
          // here you specify git credentials to be used when pulling or cloning the repo
          credentials("git-credentials")
        }

        // use master branch by default
        branch("master")
        extensions {
          localBranch("master")
        }
      }
    }
  steps {
      shell("""
npm install
npm install axios
cd ./src
vue build
      """)
    }
  
}

job("develop-deploy") {
   
	scm {
      git {
        remote {
          url("git@bitbucket.org:stacyengineering/jenkins-dsls-server-side.git")
          // here you specify git credentials to be used when pulling or cloning the repo
          credentials("git-credentials")
        }

        // use master branch by default
        branch("configuration-mgnt")
        extensions {
          localBranch("configuration-mgnt")
        }
      }
    }
    wrappers {
      credentialsBinding {
        file("BACKEND_CONFIG", "develop_backend_config")
      }
    }
    //todo: add commit number to test suite, add coverage data
    steps {
      shell("""
if ! [ -d ./venv-server-app ]; 
   then virtualenv -p python3.7 venv-server-app;
fi

. ./venv-server-app/bin/activate
pip install -r requirements.txt
BUILD_ID=dontKillMe nohup python server.py &
      """)
    }
}


job ("develop-api-tests") {
   
	scm {
      git {
        remote {
          url("git@bitbucket.org:stacyengineering/jenkins-dsls-automated-tests.git")
          // here you specify git credentials to be used when pulling or cloning the repo
          credentials("git-credentials")
        }

        // use master branch by default
        branch("master")
        extensions {
          localBranch("master")
        }
      }
    }
    wrappers {
      credentialsBinding {
        file("BACKEND_CONFIG", "BACKEND_CONFIG")
      }
    }
    //todo: add commit number to test suite, add coverage data
    steps {
      shell("""
if ! [ -d ./venv-auto-tests ]; 
   then virtualenv -p python3.7 venv-auto-tests;
fi

Xvfb :99 -ac -screen 0 1280x1024x24 &
export DISPLAY=:15

. ./venv-auto-tests/bin/activate
pip install -r requirements.txt
pytest tests/api
      """)
    }

}

pipelineJob ("ptaq-develop-pipeline") {
  definition {
    cps {
      script("""
stage('unittests') {
  build job: 'develop-unittests'
}
stage('ui-build') {
  build job: 'develop-ui-build'
}
stage('backend-deploy') {
  build job: 'develop-deploy'
}
stage('api-tests') {
  build job: 'develop-api-tests'
}
        """)
    }
  }
}
