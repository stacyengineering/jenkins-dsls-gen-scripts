def feedNames = ['develop', 'alpha', 'beta', 'prod']


feedNames.each { feedName -> 
  job("${feedName}-unittests") {
      def path_to_run = "tests"
      // handle jobs that have 'parameters' list specified
      // if you have custom test job (see above), leave the 
      // parameters below as is:
      parameters {
        // this setup is commonly used for custom tests job:
        gitParam("branch") {
          type("BRANCH")
          defaultValue("configuration-mgnt")
          description("Branch to run tests on")
        }
        stringParam('TEST_PATH', path_to_run, "select a path to run the tests")
      }
      scm {
        git {
          remote {
            url("git@bitbucket.org:stacyengineering/jenkins-dsls-server-side.git")
            // here you specify git credentials to be used when pulling or cloning the repo
            credentials("git-credentials")
          }
  
          // use master branch by default
          branch("master")
          extensions {
            localBranch("master")
          }
        }
      }
      //todo: add commit number to test suite, add coverage data
      steps {
        shell("""
  if ! [ -d ./venv-server-app ]; 
     then virtualenv -p python3.7 venv-server-app;
  fi
  
  . ./venv-server-app/bin/activate
  pip install -r requirements.txt
  
pytest -o junit_suite_name=${feedName}-unittest --junitxml=junit_report.xml -s ${path_to_run}
        """)
      }
  
  }
  
  job("${feedName}-ui-build") {
    scm {
        git {
          remote {
            url("git@bitbucket.org:stacyengineering/jenkins-dsls-frontend.git")
            // here you specify git credentials to be used when pulling or cloning the repo
            credentials("git-credentials")
          }
  
          // use master branch by default
          branch("master")
          extensions {
            localBranch("master")
          }
        }
      }
    steps {
        shell("""
  npm install
  npm install axios
  cd ./src
  vue build
        """)
      }
    
  }
  
  job("${feedName}-deploy") {
     
      scm {
        git {
          remote {
            url("git@bitbucket.org:stacyengineering/jenkins-dsls-server-side.git")
            // here you specify git credentials to be used when pulling or cloning the repo
            credentials("git-credentials")
          }
  
          // use master branch by default
          branch("configuration-mgnt")
          extensions {
            localBranch("configuration-mgnt")
          }
        }
      }
      wrappers {
        credentialsBinding {
          file("BACKEND_CONFIG", "${feedName}_backend_config")
        }
      }
      //todo: add commit number to test suite, add coverage data
      steps {
        shell("""
  if ! [ -d ./venv-server-app ]; 
     then virtualenv -p python3.7 venv-server-app;
  fi
  
  . ./venv-server-app/bin/activate
  pip install -r requirements.txt
  BUILD_ID=dontKillMe nohup python server.py &
        """)
      }
  
  }
  
  
  def testJobs = [
    "${feedName}-api-tests": [
          'path_to_run': 'tests/api'
      ],
  ]
  
  
  testJobs.each { jobName, jobConfig -> 
    job ("${jobName}") {
    //job("develop-ui-tests") {
     
      scm {
        git {
          remote {
            url("git@bitbucket.org:stacyengineering/jenkins-dsls-automated-tests.git")
            // here you specify git credentials to be used when pulling or cloning the repo
            credentials("git-credentials")
          }
  
          // use master branch by default
          branch("master")
          extensions {
            localBranch("master")
          }
        }
      }
      wrappers {
        credentialsBinding {
          file("BACKEND_CONFIG", "${feedName}_backend_config")
        }
      }
      //todo: add commit number to test suite, add coverage data
      steps {
        shell("""
  if ! [ -d ./venv-auto-tests ]; 
     then virtualenv -p python3.7 venv-auto-tests;
  fi
  
  Xvfb :99 -ac -screen 0 1280x1024x24 &
  export DISPLAY=:15
  
  . ./venv-auto-tests/bin/activate
  pip install -r requirements.txt
  pytest ${jobConfig.path_to_run}
        """)
      }
  
   }
  }
}

feedNames.each { feedName -> 
    pipelineJob ("ptaq-${feedName}-pipeline") {
    definition {
      cps {
        script("""
stage('unittests') {
  build job: '${feedName}-unittests'
}
stage('ui-build') {
  build job: '${feedName}-ui-build'
}
stage('backend-deploy') {
  build job: '${feedName}-deploy'
}
stage('api-tests') {
  build job: '${feedName}-api-tests'
}
        """)
      }
    }
  }
}