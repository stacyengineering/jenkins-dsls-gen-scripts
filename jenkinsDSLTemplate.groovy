// TEMPLATE TESTS PLUGIN:
// This is an template file that helps to create multiple jobs using
// single groovy script and dsl plugin. 
//
// You can just copy it and modify to represent your tests. :)


// specify full list of jobs you want to run:
def testJobs = [
  // regular job definition format:
  // e.g alpha-doorbird-api-tests
  '<feed>-job-name': [
    'path_to_run': 'tests or tests/something',
    'parameters': [ ... any job params ... ]
  ],
  // custom tests job (the one that will allow you to run tests from any branch):
  'custom-job-name': [
    'path_to_run': null,
    'parameters': [
      "branch": "Pick a branch",
      "test_path": "tests",
      "feed": "development"
    ]
  ]
]
// iterate over jobs and setup their configuration:
testJobs.each {jobName, jobConfig -> 
  job ("${jobName}") {

    parameters {
      // specify this parameter if you want to be able to re-run previously failed
      // tests only and not rerun the ones that previously passed:
      booleanParam('RERUN_FAILED', false, 'uncheck to run all tests')
    }
    if (jobConfig.parameters) {
      // handle jobs that have 'parameters' list specified
      // if you have custom test job (see above), leave the 
      // parameters below as is:
      parameters {
        // this setup is commonly used for custom tests job:
        if (jobConfig.parameters.branch) {
          gitParam("branch") {
            type("BRANCH")
            defaultValue("master")
            description(jobConfig.parameters.branch)
          }
        }
        if (jobConfig.parameters.test_path) {
          stringParam('TEST_PATH', "${jobConfig.parameters.test_path}", "select a path to run the tests")
        }
      }
    }
    // you can place any other 'ifs' here to specify more conditions on setting
    // parameters. Example for inspiration: 
    //    if (jobName != "projectname-ui-tests") {
    //     parameters {
    //       stringParam("BROWSER", "chrome", "Select browser for UI tests")
    //    }   
    // }
    // in the example above you don't specify the browser for api tests job

    // specify where the tests will be run:
    // todo: label is depricated, use new version instead!
    label(labelToUse)

    // specify which repo to use to clone/update tests:
    scm {
      git {
        remote {
          url("https://git.some.org/projectname/{input-repository-name-here}.git")
          
          // here you specify git credentials to be used when pulling or cloning the repo
          credentials("testing-git")
        }

        // set a branch parameter to be able to set branch before running the job:
        if (jobConfig.parameters.branch) {
          branch("\$BRANCH")
          extensions {
            localBranch("\$BRANCH")
          }
        }

        // use master branch by default
        if (!jobConfig.parameters.branch) {
          branch("master")
          extensions {
            localBranch("master")
          }
        }
      }
    }

    // specify automatic runs for given test jobs:
    if (jobName=="name-of-job") {
      triggers {
        // for nightly build from Monday to Saturday at 6AM:
        cron("00 06 * * 1-6")
        // For more information check these pages:
        // http://blogs.microsoft.co.il/meravk/2015/06/10/how-to-schedule-a-build-in-jenkins/
        // https://en.wikipedia.org/wiki/Cron
      }
    }

    // To store some settings on Jenkins, secret file format is commonly used.
    // It's very convenient to store credentials and secret information that you
    // don't want to show in logs or find on jenkins machine.
    // To add a new credentials follow the instructions here:
    // https://support.cloudbees.com/hc/en-us/articles/203802500-Injecting-Secrets-into-Jenkins-Build-Jobs
    // Code below adds bindings between saved credentials and env variables
    // used in the tests:  
    wrappers {
      credentialsBinding {
        file('NAME_OF_VAR_IN_TESTS', 'NAME_OF_SECRET_FILE_ADDED_IN_JENKINS')
        // you can coniditionally define which env variables to use in each job
        // for example:
        if (jobName.startsWith('{some feed name here}')) {
          //...
        }
      }
    }
    // Section below called 'steps' specify the actual logic of the job
    steps {
      // you can call some functions here, but you'll need to specify 
      // 'delegate' keyword. For example, if there is a function called
      // 'installDependencies':
      // def installDependencies (def context) {
      //   context.shell("pip install -r requirements.txt")
      // }
      // to call this function, specify 'delegate' to give it context
      // of current job:
      // installDependencies(delegate)
      shell("one-liner shell commands here")
      shell("""
multiliner shell commands here (please, note, that indentation matters here!)
      """)

      // Prepares test set info for ALM integration
      // Exports the following environment variables 
      // $SPRINT_NAME, $TEST_SET_FOLDER and $TEST_SET_NAME
     
      // Run tests using pipenv and pytest
      shell("""
pipenv install
ARGS='';if [ \$RERUN_FAILED = true ]; then ARGS+=' --last-failed'; fi;
pipenv run pytest -o junit_suite_name=\$TEST_SET_NAME --junitxml=junit_report.xml -s ${job_config.path_to_run} --alluredir ./allure-results \$ARGS;
      """)
    }

    // Publishers section is responsible for different post-build actions,
    // like reporting, email sending etc.
    // You can add different plugins here. For list of available plugins and
    // their groovy representations, refer here:
    // https://jenkinsci.github.io/job-dsl-plugin/#
    publishers {
      // to publish junit_reports use (please,note that you'll have to 
      // generate actual .xml file using your testing framework during test run).
      // Specify same file name as you are generating in tests:
      archiveJunit('junit_report.xml')

      // to publish results in ALM:
      testResultToALMUploader {
        // do not change --------:
        almServerName("ALM 12.5")
        clientType("REST Client")
        credentialsId("id-of-credentials")
        almDomain("DEFAULT")
        almProject("DSSWebsite")
        testingFramework("JUnit")
        testingTool("")
        // -----------------------
        // specify folder where you want your test instances to be created in ALM
        // Test Plan. Please note, that you have to use "\" slashes and escape them
        // with another "\". Also please note that you should not mention root
        // folder (in case with test lab it's called "Subject"):
        // almTestFolder("Path\\To\\Tests")
        almTestFolder("$teamName\\$viewName\\$jobNameNoFeed")
        // specify folder in ALM Test Lab. You can use "SPRINT_NUMBER" parameter (see
        // in parameters section above) to specify sprint number. Also, do NOT mention
        // "Root" folder in path:
        // almTestSetFolder("dS Development\\<Team Name>\\<Sprint Number>\\<View Name>")
        almTestSetFolder("Folder name\\$teamName\\\$SPRINT_NAME\\\$TEST_SET_FOLDER")
        // specify testing result file here starting with **/, or just all xml files
        // in jenkins workspace (plugin will understand which one is your junit report)
        testingResultFile("**/junitResult.xml")
        // do not change ---------:
        jenkinsServerUrl("\$JENKINS_URL")
        almTimeout("")
        // ------------------------
      }

      // for allure reports specify an empty allure section:
      allure {}

      // to send emails on job end, sepcify extendedEmail section:
      extendedEmail {
          recipientList('email@digitalstrom.com')
          // specify email title (example presented below):
          defaultSubject('Jenkins build failed: \$JOB_NAME [\$BUILD_NUMBER]')

          // ... and email content (example presented below):
          defaultContent("""
<h4> Job \$JOB_NAME [\$BUILD_NUMBER] FAILED:</h4> <br/>

Check error report at \$JOB_URL\$BUILD_NUMBER/allure <br/>

Check logs at \$JOB_URL\$BUILD_NUMBER/console <br/>
          """)
          
          // just leave this format as is: 
          contentType('text/html')

          // specify what triggers an email:
          triggers {
            beforeBuild()

            // to send on failure:
            failure {
              sendTo {
                // to send email to recepient list specified above, use:
                recipientList()
                // these options can be used in the future to send
                // messages to all dev team:
                developers()
                // ... or the one who triggered build:
                requester()
              }
            }
          }
        }
      }
      
  } 

} 
